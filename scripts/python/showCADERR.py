import pifacecad
import sys

cad = pifacecad.PiFaceCAD()
cad.lcd.clear()
cad.lcd.write('Error\n')
cad.lcd.write(sys.argv[1])
cad.lcd.backlight_on()
cad.lcd.cursor_off()
cad.lcd.blink_off()
