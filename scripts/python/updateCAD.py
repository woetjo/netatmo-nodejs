#!/usr/bin/python
import sys
import pifacecad

celsius = pifacecad.LCDBitmap([0x0, 0xc, 0xc, 0x0, 0x0, 0x0, 0x0, 0x0])
up_arr = pifacecad.LCDBitmap([0x0, 0x0, 0x4, 0xe, 0x1f, 0x0, 0x0, 0x0])
down_arr = pifacecad.LCDBitmap([0x0, 0x0, 0x1f, 0xe, 0x4, 0x0, 0x0, 0x0])
cad = pifacecad.PiFaceCAD()
cad.lcd.backlight_on()
cad.lcd.store_custom_bitmap(0, celsius)
cad.lcd.store_custom_bitmap(1, up_arr)
cad.lcd.store_custom_bitmap(2, down_arr)
cad.lcd.clear()
cad.lcd.write('In  ')
if sys.argv[2] == 'up':
  cad.lcd.write_custom_bitmap(1)
if sys.argv[2] == 'down':
  cad.lcd.write_custom_bitmap(2)
if sys.argv[2] == 'stable':
  cad.lcd.write(' ')
cad.lcd.write(sys.argv[1])
cad.lcd.write_custom_bitmap(0)
cad.lcd.write(', ')
cad.lcd.write(sys.argv[3])
cad.lcd.write('%')
cad.lcd.write('\n')
cad.lcd.write('Out ')
if sys.argv[5] == 'up':
  cad.lcd.write_custom_bitmap(1)
if sys.argv[5] == 'down':
  cad.lcd.write_custom_bitmap(2)
if sys.argv[5] == 'stable':
  cad.lcd.write(' ')
cad.lcd.write(sys.argv[4])
cad.lcd.write_custom_bitmap(0)
cad.lcd.write(', ')
cad.lcd.write(sys.argv[6])
cad.lcd.write('%')
cad.lcd.blink_off()
cad.lcd.cursor_off()

# cad.lcd.backlight_on()
# time.sleep (5)
# cad.lcd.backlight_off()
# cad.lcd.cursor_off()
# cad.lcd.blink_off()
# cad.lcd.clear()
# print('hoi')
# sys.stdout.flush()
