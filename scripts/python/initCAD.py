import pifacecad

cad = pifacecad.PiFaceCAD()
cad.lcd.clear()
cad.lcd.write('NetAtmo\ninitializing...')
cad.lcd.backlight_on()
cad.lcd.cursor_off()
cad.lcd.blink_off()
