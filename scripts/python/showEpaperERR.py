#!/usr/bin/python
import sys

sys.path.append('/home/pi/ws/epaper/RaspberryPi/python2')
from datetime import datetime
import epd2in7
import time
from PIL import Image, ImageDraw, ImageFont

epd = epd2in7.EPD()
epd.init()

# WIDTH 264, HEIGHT 176
Himage = Image.new('1', (epd2in7.EPD_HEIGHT, epd2in7.EPD_WIDTH), 255)
draw = ImageDraw.Draw(Himage)

font14 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 14)
font16 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 16)

# time & title
draw.text((180, 0), datetime.now().strftime("%H:%M:%S"), font=font16, fill=0)
draw.text((5, 0), 'Error', font=font16, fill=0)

# DIVIDER 1
draw.line((5, 20, 259, 20), fill=0)

# ERROR
draw.text((5, 40), sys.argv[1] + ' ', font=font14, fill=0)

# DIVIDER 3
draw.line((5, 156, 259, 156), fill=0)

# DRAW ON EPAPER
Himage = Himage.rotate(180, expand=True)
epd.display(epd.getbuffer(Himage))
time.sleep(2)
