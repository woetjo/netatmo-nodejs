#!/usr/bin/python
import sys

sys.path.append('/home/pi/ws/epaper/RaspberryPi/python2')
from datetime import datetime
import epd2in7
import time
from PIL import Image, ImageDraw, ImageFont

epd = epd2in7.EPD()
epd.init()
# epd.Clear(0xFF)

# WIDTH 264, HEIGHT 176
Himage = Image.new('1', (epd2in7.EPD_HEIGHT, epd2in7.EPD_WIDTH), 255)
draw = ImageDraw.Draw(Himage)

font14 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 14)
font16 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 16)
font20 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 20)
font24 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 24)
font40 = ImageFont.truetype('/usr/share/fonts/truetype/dejavu/DejaVuSansMono-Bold.ttf', 40)

# time & title
draw.text((180, 0), datetime.now().strftime("%H:%M:%S"), font=font16, fill=0)
draw.text((5, 0), 'Netatmo', font=font16, fill=0)

# DIVIDER 1
draw.line((5, 20, 259, 20), fill=0)

# BINNEN
draw.text((118, 35), 'o', font=font16, fill=0)
draw.text((20, 25), 'Huiskamer', font=font14, fill=0)
draw.text((20, 40), sys.argv[1] + ' ', font=font40, fill=0)
draw.text((160, 25), 'Humidity', font=font14, fill=0)
draw.text((160, 40), sys.argv[3] + '%', font=font40, fill=0)

# DIVIDER 2
draw.line((5, 88, 259, 88), fill=0)

# BUITEN
draw.text((118, 103), 'o', font=font16, fill=0)
draw.text((20, 93), 'Buiten', font=font14, fill=0)
draw.text((20, 108), sys.argv[4] + ' ', font=font40, fill=0)
draw.text((160, 93), 'Humidity', font=font14, fill=0)
draw.text((160, 108), sys.argv[6] + '%', font=font40, fill=0)

# DIVIDER 3
draw.line((5, 156, 259, 156), fill=0)

# DRAW ON EPAPER
Himage = Himage.rotate(180, expand=True)
epd.display(epd.getbuffer(Himage))
time.sleep(2)
