#!/usr/bin/python
import sys

sys.path.append('/home/pi/ws/epaper/RaspberryPi/python2')
from datetime import datetime
import epd2in7
import time
from PIL import Image, ImageDraw, ImageFont

epd = epd2in7.EPD()
epd.init()
epd.Clear(0xFF)
time.sleep(2)
