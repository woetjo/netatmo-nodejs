'use strict';

const https = require('https');
const FormData = require('form-data');
const spawn = require("child_process").spawn;
const {createLogger, format} = require('winston');
const {combine, timestamp, printf} = format;
const winston = require('winston');
const moment = require('moment');
const output = "epaper" // "epaper" or "cad"
require('dotenv').config();

let request_interval = (1000 * 60) * 10 // min
let refresh_interval = (1000 * 60) * 120 // min
let access_token;
let refresh_token;
let weatherData;

const timestamps = () => moment().format('YYYY-MM-DD HH:mm:ss');
const myFormat = printf(({level, message, timestamp}) => {
  return `${timestamps()} ${level}: ${message}`;
});
const logger = createLogger({
  format: combine(
    timestamp(),
    myFormat
  ),
  transports: [
    new winston.transports.File({
      filename: 'netatmo.log',
      level: 'info'
    }),
  ]
});

function main() {
  getStationData();

  setInterval(function () {
    getStationData();
  }, request_interval);

  setInterval(function () {
    refreshToken();
  }, refresh_interval)
}

function refreshToken() {
  const form = new FormData();
  form.append('grant_type', 'refresh_token');
  form.append('refresh_token', refresh_token);
  form.append('client_id', process.env.client_id);
  form.append('client_secret', process.env.client_secret);

  const refreshRequest = https.request({
    host: 'api.netatmo.com',
    path: '/oauth2/token',
    method: 'POST',
    headers: form.getHeaders()
  })

  form.pipe(refreshRequest);

  refreshRequest.on('response', function (res) {
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      try {
        const response = JSON.parse(chunk);
        access_token = response['access_token'];
        refresh_token = response['refresh_token'];
      } catch (e) {
        showError('Refreshing token failed')
      }
    });
  });

  refreshRequest.on('error', error => {
    logger.error('Refresh token error: ' + error);
    showError(error);
  })

  refreshRequest.end();
}

function getStationData() {
  const options = {
    host: 'api.netatmo.com',
    port: 443,
    path: '/api/getstationsdata',
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + access_token
    }
  };

  const getDataRequest = https.request(options, res => {
    res.on('data', d => {
      try {
        const data = JSON.parse(d).body;
        if (data.devices[0].hasOwnProperty('dashboard_data')) {
          weatherData = {
            temp_in: Number(data.devices[0].dashboard_data.Temperature).toFixed(1) || '--.-',
            temp_in_trend: data.devices[0].dashboard_data.temp_trend || 'stable',
            humidity_in: data.devices[0].dashboard_data.Humidity || '--',
            temp_out: Number(data.devices[0].modules[0].dashboard_data.Temperature).toFixed(1) || '--.-',
            temp_out_trend: data.devices[0].modules[0].dashboard_data.temp_trend || 'stable',
            humidity_out: data.devices[0].modules[0].dashboard_data.Humidity || '--'
          };
          if (weatherData.temp_out !== '--.-' && weatherData.temp_out < 10 && weatherData.temp_out >= 0) {
            weatherData.temp_out = ' ' + weatherData.temp_out;
          }

        } else {
          weatherData = {
            temp_in: '--.-',
            temp_in_trend: 'stable',
            humidity_in: '--',
            temp_out: '--.-',
            temp_out_trend: 'stable',
            humidity_out: '--'
          };
        }
        updateScreen(weatherData);
      } catch (e) {
        showError('No data from station')
      }
    })
  })

  getDataRequest.on('error', error => {
    logger.error('Stationdata error: ' + error)
    showError(error);
  })
  getDataRequest.end();
}

function initScreen() {
  if (output === 'cad') {
    const pythonProcess = spawn('python', ["scripts/python/initCAD.py"]);
    pythonProcess.stdout.on('data', (data) => {
      // Do something with the data returned from python script
    });
  } else if (output === 'epaper') {
    const pythonProcess = spawn('python', ["scripts/python/initEpaper.py"]);
    pythonProcess.stdout.on('data', (data) => {
      // Do something with the data returned from python script
    });
  }
}

function updateScreen(weatherData) {
  if (output === 'cad') {
    const pythonProcess = spawn('python', ["scripts/python/updateCAD.py", weatherData.temp_in, weatherData.temp_in_trend, weatherData.humidity_in, weatherData.temp_out, weatherData.temp_out_trend, weatherData.humidity_out]);
    // const pythonProcess = spawn('python',["scripts/python/updateEpaper.py", temp_in, temp_in_trend, humidity_in, temp_out, temp_out_trend, humidity_out]);
    pythonProcess.stdout.on('data', (data) => {
      // Do something with the data returned from python script
    });
  } else if (output === 'epaper') {
    const pythonProcess = spawn('python', ["scripts/python/updateEpaper.py", weatherData.temp_in, weatherData.temp_in_trend, weatherData.humidity_in, weatherData.temp_out, weatherData.temp_out_trend, weatherData.humidity_out]);
    pythonProcess.stdout.on('data', (data) => {
      // Do something with the data returned from python script
    });
  }

}

function showError(error) {
  if (output === 'cad') {
    const pythonProcess = spawn('python', ["scripts/python/showCADERR.py", error]);
    pythonProcess.stdout.on('data', (data) => {
      // Do something with the data returned from python script
    });
  } else if (output === 'epaper') {
    const pythonProcess = spawn('python', ["scripts/python/showEpaperERR.py", error]);
    pythonProcess.stdout.on('data', (data) => {
      // Do something with the data returned from python script
    });
  }

}

// *****************************************************************************
// Starting point
initScreen();

const form = new FormData();
form.append('grant_type', 'password');
form.append('client_id', process.env.client_id);
form.append('client_secret', process.env.client_secret);
form.append('username', process.env.username);
form.append('password', process.env.password);
form.append('scope', 'read_station');

const getTokenRequest = https.request({
  host: 'api.netatmo.com',
  path: '/oauth2/token',
  method: 'POST',
  headers: form.getHeaders()
})

form.pipe(getTokenRequest);

getTokenRequest.on('response', function (res) {
  res.setEncoding('utf8');
  res.on('data', function (chunk) {
    const response = JSON.parse(chunk);
    access_token = response['access_token'];
    refresh_token = response['refresh_token'];
    main();
  });
});

getTokenRequest.on('error', error => {
  logger.error('Authorization error: ' + error)
  showError(error);
})


